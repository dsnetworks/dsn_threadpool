#include <dsn/threadpool/detail/task.h>

/**
 * @brief Default constructor
 */
dsn::threadpool::detail::task::task()
    : m_callback()
    , m_priority()
    , m_order()
{
}

/**
 * @brief Construct new task
 * @param callback Callable to use for the new task
 * @param priority Priority value for this task
 * @param order Counter for task orderin
 */
dsn::threadpool::detail::task::task(dsn::threadpool::detail::task::callback_t                   callback,
                                    dsn::threadpool::detail::task::priority_t                   priority,
                                    infinite_counter<dsn::threadpool::detail::task::priority_t> order)
    : m_callback{ std::move(callback) }
    , m_priority(priority)
    , m_order{ std::move(order) }
{
}

/**
 * @brief Priority comparison operator
 * @param rhs Task against which the priority shall be compared
 * @return true if this task has a higher priority than `rhs`, otherwise false
 */
bool dsn::threadpool::detail::task::operator<(const dsn::threadpool::detail::task& rhs) const
{
    if (m_priority == rhs.m_priority) {
        return m_order > rhs.m_order;
    }

    return m_priority < rhs.m_priority;
}
