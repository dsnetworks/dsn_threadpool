#include <dsn/threadpool/error.h>
#include <dsn/threadpool/pool.h>

/**
 * @brief Default constructor
 *
 * Constructs an empty thread pool without any active workers. Use `add_threads` to add workers after
 * construction.
 *
 * @see add_threads
 */
dsn::threadpool::pool::pool()
    : m_done{ false }
    , m_paused{ false }
    , m_threads{}
    , m_thread_mutex{}
    , m_tasks{}
    , m_task_mutex{}
    , m_task_cond{}
    , m_task_counter{}
{
}

/**
 * @brief Construct with pool size
 *
 * Constructs a new pool with `num_threads` workers to execute tasks.
 *
 * @param num_threads Number of worker threads to launch for the newly created pool
 */
dsn::threadpool::pool::pool(std::size_t num_threads)
    : pool()
{
    if (num_threads > 0) {
        guard_t    guard(m_thread_mutex);
        const auto target = m_threads.size() + num_threads;
        while (m_threads.size() < target) {
            std::thread thread;
            try {
                thread = std::thread{ &pool::worker, this };
            }

            catch (...) {
                shutdown();
                throw;
            }

            try {
                m_threads.push_back(std::move(thread));
            } catch (...) {
                shutdown();
                thread.join();
                throw;
            }
        }
    }
}

/// @brief Destroy thread pool
dsn::threadpool::pool::~pool() { shutdown(); }

/**
 * @brief Add more worker threads to pool
 *
 * Spawns `num_threads` worker threads to execute tasks on the pool.
 *
 * @see worker
 *
 * @param num_threads Number of worker threads to spawn
 */
void dsn::threadpool::pool::add_threads(std::size_t num_threads)
{
    if (num_threads == 0) {
        return;
    }

    guard_t task_lock(m_task_mutex);
    if (m_done) {
        throw dsn::threadpool::error("add_threads called while pool is shutting down");
    }

    guard_t    thread_lock(m_thread_mutex);
    const auto target = m_threads.size() + num_threads;
    while (m_threads.size() < target) {
        std::thread thread(&pool::worker, this);
        try {
            m_threads.push_back(std::move(thread));
        } catch (...) {
            shutdown();
            thread.join();
            throw;
        }
    }
}

/// @brief Get number of worker threads
std::size_t dsn::threadpool::pool::num_threads() const
{
    guard_t task_lock(m_task_mutex);
    if (m_done) {
        throw dsn::threadpool::error("num_threads called while pool is shutting down");
    }

    guard_t thread_lock(m_thread_mutex);
    return m_threads.size();
}

/// @brief Get number of queued tasks
std::size_t dsn::threadpool::pool::num_tasks() const
{
    guard_t guard(m_task_mutex);
    return m_tasks.size();
}

/// @brief Clear task queue
void dsn::threadpool::pool::clear()
{
    guard_t           guard(m_task_mutex);
    decltype(m_tasks) empty;

    m_tasks.swap(empty);
}

/**
 * @brief Pause/resume task processing
 * @param enabled Boolean flag indicating whether task processing shall be paused (`true`) or resumed (`false`)
 */
void dsn::threadpool::pool::set_paused(bool enabled)
{
    {
        guard_t guard(m_task_mutex);
        m_paused = enabled;
    }

    if (!m_paused) {
        m_task_cond.notify_all();
    }
}

/// @brief Check if task processing is paused
bool dsn::threadpool::pool::is_paused() const
{
    guard_t guard(m_task_mutex);
    return m_paused;
}

/**
 * @brief Worker thread entry point
 *
 * This function is used as the entry point for the pool's worker thread. It fetches tasks from `m_tasks` and
 * executes their attached functor.
 */
void dsn::threadpool::pool::worker()
{
    for (;;) {
        detail::task task;
        {
            unique_lock_t task_lock(m_task_mutex);
            m_task_cond.wait(task_lock, [this]() { return !m_paused && (m_done || !m_tasks.empty()); });
            if (m_done && m_tasks.empty()) {
                break;
            }

            task = m_tasks.top();
            m_tasks.pop();
        }

        task();
    }
}

/**
 * @brief Shut down thread pool
 *
 * Stops task processing on the thread pool and ends all launched worker threads.
 */
void dsn::threadpool::pool::shutdown()
{
    {
        guard_t task_lock(m_task_mutex);
        m_done   = true;
        m_paused = false;
    }

    m_task_cond.notify_all();
    guard_t thread_lock(m_thread_mutex);
    for (auto& thread : m_threads) {
        thread.join();
    }
}
