#include <dsn/threadpool/error.h>

/**
 * @brief Create new exception instance
 * @param message Human-readable description of the error that caused the exception
 */
dsn::threadpool::error::error(const std::string& message)
    : m_message(message)
{
}

/**
 * @brief Get exception's error message
 * @return 0-terminated string with a human-readable error description
 */
const char* dsn::threadpool::error::what() const noexcept { return m_message.c_str(); }
