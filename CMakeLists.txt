project(dsn_threadpool)
cmake_minimum_required(VERSION 3.6)

option(dsn_threadpool_WITH_TESTS "Build unit tests for dsn_threadpool" ON)
option(dsn_threadpool_WITH_CPACK "Include CPack to generate release packages" ON)

set(dsn_threadpool_VERSION_MAJOR 0)
set(dsn_threadpool_VERSION_MINOR 99)
set(dsn_threadpool_VERSION_PATCH 1)

set(dsn_threadpool_SOURCE_ROOT ${CMAKE_CURRENT_SOURCE_DIR})
set(dsn_threadpool_BUILD_ROOT ${CMAKE_CURRENT_BINARY_DIR})

if(CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND CMAKE_CXX_COMPILER_VERSION GREATER "5.0.0")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17 -stdlib=libc++")
    include_directories(${LLVM_ROOT}/include/c++/v1)
    link_directories(${LLVM_ROOT}/lib)
    set(dsn_threadpool_WITH_COMPILE_FEATURES FALSE)
else(CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND CMAKE_CXX_COMPILER_VERSION GREATER "5.0.0")
    set(dsn_threadpool_WITH_COMPILE_FEATURES TRUE)
endif(CMAKE_CXX_COMPILER_ID STREQUAL "Clang" AND CMAKE_CXX_COMPILER_VERSION GREATER "5.0.0")

if(dsn_threadpool_WITH_TESTS)
    find_package(Boost REQUIRED COMPONENTS unit_test_framework)
endif(dsn_threadpool_WITH_TESTS)

install(DIRECTORY include/dsn DESTINATION include)

add_subdirectory(src)

install(EXPORT dsn_threadpool-targets NAMESPACE dsn:: DESTINATION share/cmake/)

include(CMakePackageConfigHelpers)
write_basic_package_version_file(
    "${CMAKE_CURRENT_BINARY_DIR}/share/cmake/dsn_threadpool-version.cmake"
    VERSION ${dsn_threadpool_VERSION_MAJOR}.${dsn_threadpool_VERSION_MINOR}.${dsn_threadpool_VERSION_PATCH}
    COMPATIBILITY AnyNewerVersion)
install(FILES
    cmake/dsn_threadpool-config.cmake
    "${CMAKE_CURRENT_BINARY_DIR}/share/cmake/dsn_threadpool-version.cmake"
    DESTINATION share/cmake)

if(dsn_threadpool_WITH_TESTS)
    enable_testing()
    include(CTest)
    add_subdirectory(tests)
endif(dsn_threadpool_WITH_TESTS)

if(dsn_threadpool_WITH_CPACK)
    set(CPACK_PACKAGE_VERSION_MAJOR ${dsn_threadpool_VERSION_MAJOR})
    set(CPACK_PACKAGE_VERSION_MINOR ${dsn_threadpool_VERSION_MINOR})
    set(CPACK_PACKAGE_VERSION_PATCH ${dsn_threadpool_VERSION_PATCH})
    include(CPack)
endif(dsn_threadpool_WITH_CPACK)

