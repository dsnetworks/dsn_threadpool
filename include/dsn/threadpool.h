// -*- C++ -*-
#pragma once

#include "threadpool/pool.h"
#include "threadpool/wait.h"

namespace dsn {
/// @brief C++11 Thread pool
namespace threadpool {
    /**
     * @brief Thread pool implementation details
     *
     * This namespace contains various helper templates and classes that are used by the thread pool.
     */
    namespace detail {
    } // namespace detail
} // namespace threadpool
} // namespace dsn
