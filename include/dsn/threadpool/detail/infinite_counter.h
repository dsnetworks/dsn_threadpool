// -*- C++ -*-
#pragma once

#include <limits>
#include <vector>

namespace dsn {
namespace threadpool {
    namespace detail {
        /**
         * @brief Infinite counter
         *
         * This template provides a counter that can count up to arbitrarily high values.
         *
         * @tparam T Integral type to use for counter values
         * @tparam max Maximum value that one `T` can hold
         */
        template <typename T, T max = std::numeric_limits<T>::max()> class infinite_counter {
            static_assert(std::is_integral<T>::value, "T needs to be an integral type");

        public:
            infinite_counter();
            infinite_counter& operator++();
            bool              operator>(const infinite_counter& rhs) const;

        private:
            std::vector<T> m_count; //!< Storage for counter value
        };

        /**
         * @brief Default constructor
         *
         * Initializes the counter to zero.
         */
        template <typename T, T max>
        infinite_counter<T, max>::infinite_counter()
            : m_count{ 0 }
        {
        }

        /// @brief Increase counter value
        template <typename T, T max> infinite_counter<T, max>& infinite_counter<T, max>::operator++()
        {
            if (m_count.back() == max) {
                m_count.push_back(0);
            } else {
                ++m_count.back();
            }

            return *this;
        }

        /**
         * @brief Comparison operator (greater than)
         *
         * @param rhs Counter against which this one shall be compared
         *
         * @return true If this counter's value is higher than that of `rhs`
         */
        template <typename T, T max> bool infinite_counter<T, max>::operator>(const infinite_counter& rhs) const
        {
            if (m_count.size() == rhs.m_count.size()) {
                return m_count.back() > rhs.m_count.back();
            }

            return m_count.size() > rhs.m_count.size();
        }
    } // namespace detail
} // namespace threadpool
} // namespace detail
