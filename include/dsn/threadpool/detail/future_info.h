// -*- C++ -*-
#pragma once

#include <iterator>
#include <type_traits>

namespace dsn {
namespace threadpool {
    namespace detail {
        /// @brief Metaprogramming helper for information about a future
        template <typename T> struct future_info {

            /// @brief Type of future
            typedef typename std::iterator_traits<T>::value_type future_type;

            /// @brief Type of future's result
            typedef typename std::result_of<decltype (&future_type::get)(future_type)>::type value_type;

            /// @brief Boolean flag indicating whether `value_type` is `void`
            static constexpr bool is_void = std::is_void<value_type>::value;
        };
    } // namespace detail
} // namespace threadpool
} // namespace dsn
