// -*- C++ -*-
#pragma once

#include "infinite_counter.h"

#include <dsn/threadpool/exports.h>

#include <cstddef>
#include <functional>

namespace dsn {
namespace threadpool {
    namespace detail {
        /**
         * @brief Task for execution on pool
         *
         * This wraps a callable and a numeric priority for a single job that is to be executed on a pool.
         */
        class DSN_THREADPOOL_EXPORT task {
        public:
            typedef std::size_t           priority_t; //!< Type alias for numeric priority
            typedef std::function<void()> callback_t; //!< Type alias for task callable

            task();
            task(callback_t callback, priority_t priority, infinite_counter<priority_t> order);

            bool operator<(const task& rhs) const;

            inline void operator()() const { m_callback(); }

        private:
            callback_t                   m_callback; //!< Callback for this task
            priority_t                   m_priority; //!< Priority of this task
            infinite_counter<priority_t> m_order;    //!< Counter for task order
        };
    } // namespace detail
} // namespace threadpool
} // namespace dsn
