// -*- C++ -*-
#pragma once

#include "detail/future_info.h"

#include <chrono>

namespace dsn {
namespace threadpool {
    template <typename Iterator> void wait(Iterator first, Iterator last);
    template <typename Result, typename Iterator, typename Rep, typename Period>
    Result wait_for(Iterator first, Iterator last, const std::chrono::duration<Rep, Period>& timeout_duration,
                    Result result);
    template <typename Result, typename Iterator, typename Clock, typename Duration>
    Result wait_until(Iterator first, Iterator last, const std::chrono::time_point<Clock, Duration>& timeout_time,
                      Result result);

    /**
     * @brief Calls get() on all futures
     *
     * This helper can be used to gather the results of a container of futures. It has specializations for
     * any container that supports `push_back` and standard iterators as well as `future<void>` where
     * construction of a result container is impossible.
     *
     * @tparam Result Data type of future results, can be void
     * @tparam Iterator Iterator to a container of `std::future<Result>` instances
     *
     * @note The anonymous template parameter is used to specialize the template for `void` futures.
     */
    template <typename Result, typename Iterator, typename> Result get(Iterator first, Iterator last, Result result);

    /**
     * @brief Waits until all futures contain results
     *
     * @tparam Iterator Iterators to a container of `std::future<>` instances
     *
     * @param first Begin of futures container/range
     * @param last End of futures container/range
     */
    template <typename Iterator> void wait(Iterator first, Iterator last)
    {
        for (; first != last; ++first) {
            first->wait();
        }
    }

    /// @brief Waits until all futures contain results within a given duration
    template <typename Result, typename Iterator, typename Rep, typename Period>
    Result wait_for(Iterator first, Iterator last, const std::chrono::duration<Rep, Period>& timeout_duration,
                    Result result)
    {
        for (; first != last; ++first) {
            result.push_back(first->wait_for(timeout_duration));
        }

        return result;
    }

    /// @brief Waits until all futures contain results within a duration and returns them as vector
    template <typename Iterator, typename Rep, typename Period>
    inline std::vector<std::future_status> wait_for(Iterator first, Iterator last,
                                                    const std::chrono::duration<Rep, Period>& timeout_duration)
    {
        return wait_for(first, last, timeout_duration, std::vector<std::future_status>{});
    }

    /// @brief Waits until all futures contain results until a given time point
    template <typename Result, typename Iterator, typename Clock, typename Duration>
    Result wait_until(Iterator first, Iterator last, const std::chrono::time_point<Clock, Duration>& timeout_time,
                      Result result)
    {
        for (; first != last; ++first) {
            result.push_back(first->wait_until(timeout_time));
        }

        return result;
    }

    /// @brief Waits until all futures contain results until a given time point
    template <typename Iterator, typename Clock, typename Duration>
    inline std::vector<std::future_status> wait_until(Iterator first, Iterator last,
                                                      const std::chrono::time_point<Clock, Duration>& timeout_time)
    {
        return wait_until(first, last, timeout_time, std::vector<std::future_status>{});
    }

    /// \brief Calls get on all futures for a container of `future<void>`
    template <typename Iterator, typename = typename std::enable_if<detail::future_info<Iterator>::is_void>::type>
    void get(Iterator first, Iterator last)
    {
        for (; first != last; ++first) {
            first->get();
        }
    }

    /// \brief Calls get on all futures and stores their results in the given container
    template <typename Result, typename Iterator,
              typename = typename std::enable_if<!detail::future_info<Iterator>::is_void>::type>
    Result get(Iterator first, Iterator last, Result result)
    {
        for (; first != last; ++first) {
            result.push_back(first->get());
        }

        return result;
    }

    /// \brief Calls get on all futures and stores their results in a vector
    template <typename Iterator, typename = typename std::enable_if<!detail::future_info<Iterator>::is_void>::type>
    std::vector<typename detail::future_info<Iterator>::value_type> get(Iterator first, Iterator last)
    {
        return get(first, last, std::vector<typename detail::future_info<Iterator>::value_type>{});
    }
} // namespace threadpool
} // namespace dsn
