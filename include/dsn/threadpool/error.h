// -*- C++ -*-
#pragma once

#include <dsn/threadpool/exports.h>

#include <exception>
#include <string>

namespace dsn {
namespace threadpool {
    class DSN_THREADPOOL_EXPORT error : public std::exception {
    private:
        std::string m_message;

    public:
        explicit error(const std::string& message);

        virtual ~error() = default;

        // exception interface
    public:
        virtual const char* what() const noexcept override;
    };
}
}
