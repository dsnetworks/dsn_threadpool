// -*- C++ -*-
#pragma once

#include <dsn/threadpool/exports.h>

#include "detail/task.h"
#include "error.h"

#include <cstddef>
#include <future>
#include <queue>
#include <thread>
#include <vector>

namespace dsn {
namespace threadpool {
    /**
     * @brief Thread pool implementation
     *
     * This class provides a pool of worker threads that can execute arbitrary callables (functions, function objects,
     * lambdas, etc.) in parallel.
     */
    class DSN_THREADPOOL_EXPORT pool {
    public:
        pool();
        explicit pool(std::size_t num_threads);

        ~pool();

        pool(const pool&) = delete;
        pool& operator=(const pool&) = delete;

        pool(pool&&)  = delete;
        pool& operator=(pool&&) = delete;

        void        add_threads(std::size_t num_threads);
        std::size_t num_threads() const;

        template <typename F, typename... Args>
        auto push(F&& functor, Args&&... args) -> std::future<decltype(functor(args...))>;

        template <typename F, typename... Args>
        auto push(std::size_t priority, F&& functor, Args&&... args) -> std::future<decltype(functor(args...))>;

        std::size_t num_tasks() const;

        void clear();

        void set_paused(bool enabled);
        bool is_paused() const;

    private:
        void worker();
        void shutdown();

        bool m_done;   //!< Flag that indicates whether the pool is shutting down
        bool m_paused; //!< Flag that indicates whether task processing is currently paused

        std::vector<std::thread> m_threads;      //!< Vector for worker threas
        mutable std::mutex       m_thread_mutex; //!< Mutex to synchronize access to `m_threads`

        std::priority_queue<detail::task> m_tasks;      //!< Tasks queued for execution on pool
        mutable std::mutex                m_task_mutex; //!< Mutex to synchronize access to `m_tasks`
        std::condition_variable           m_task_cond;  //!< Condition variable to notify worker threads
        detail::infinite_counter<typename detail::task::priority_t> m_task_counter; //!< Task counters

        typedef std::lock_guard<std::mutex>  guard_t;
        typedef std::unique_lock<std::mutex> unique_lock_t;
    };

    /**
     * @brief Queue task for execution on pool
     *
     * Adds a task to the queue so it will be executed by one of the worker threads. The task is added with the
     * default priority (0).
     *
     * @param functor Callable that shall be executed as a task
     * @param args Pack of arguments for `functor`
     *
     * @tparam F Callable type that shall be executed
     * @tparam Args Pack of arguments for `F`
     *
     * @return `std::future` for the return value of `functor(args...)`
     */
    template <typename F, typename... Args>
    auto pool::push(F&& functor, Args&&... args) -> std::future<decltype(functor(args...))>
    {
        return push(0, std::forward<F>(functor), std::forward<Args>(args)...);
    }

    /**
     * @brief Queue task for execution on pool
     *
     * Adds a task to the queue so it will be executed by one of the worker threads.
     *
     * @param priority Priority of the queued task (0 = highest)
     * @param functor Callable that shall be executed as a task
     * @param args Pack of arguments for `functor`
     *
     * @tparam F Callable type that shall be executed
     * @tparam Args Pack of arguments for `F`
     *
     * @return `std::future` for the return value of `functor(args...)`
     */
    template <typename F, typename... Args>
    auto pool::push(std::size_t priority, F&& functor, Args&&... args) -> std::future<decltype(functor(args...))>
    {
        typedef decltype(functor(args...)) result_type;
        auto                               pack_task = std::make_shared<std::packaged_task<result_type()>>(
            std::bind(std::forward<F>(functor), std::forward<Args>(args)...));
        auto future = pack_task->get_future();
        {
            guard_t task_lock(m_task_mutex);
            if (m_done) {
                throw dsn::threadpool::error("push called while pool is shutting down");
            }

            m_tasks.emplace([pack_task]() { (*pack_task)(); }, priority, ++m_task_counter);
        }

        m_task_cond.notify_one();

        return future;
    }
} // namespace threadpool
} // namespace dsn
