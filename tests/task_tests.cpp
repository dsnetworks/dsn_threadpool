#define BOOST_TEST_MODULE "dsn::threadpool::detail::task"

#include <dsn/threadpool/detail/task.h>

#include <boost/test/unit_test.hpp>

using dsn::threadpool::detail::task;
using counter = dsn::threadpool::detail::infinite_counter<typename task::priority_t>;

BOOST_AUTO_TEST_CASE(default_ctoR)
{
    task t1, t2;

    BOOST_REQUIRE(!(t1 < t2));
    BOOST_REQUIRE(!(t2 < t1));
}

BOOST_AUTO_TEST_CASE(different_prios)
{
    counter c;
    task    t1([]() { /* nothing */ }, 3, c);

    ++c;
    task t2([]() { /* nothing */ }, 2, c);

    BOOST_REQUIRE(t2 < t1);
    BOOST_REQUIRE(!(t1 < t2));
}

BOOST_AUTO_TEST_CASE(same_prios)
{
    counter c;
    task    t1([]() { /* nothing */ }, 2, c);
    ++c;
    task t2([]() { /* nothing */ }, 2, c);

    BOOST_REQUIRE(t2 < t1);
    BOOST_REQUIRE(!(t1 < t2));
}

BOOST_AUTO_TEST_CASE(same_prios_same_order)
{
    counter c;
    task    t1([]() { /* nothing */ }, 2, c);
    task    t2([]() { /* nothing */ }, 2, c);
    BOOST_REQUIRE(!(t1 < t2));
    BOOST_REQUIRE(!(t2 < t1));
}
