#define BOOST_TEST_MODULE "dsn::threadpool::detail::infinite_counter"

#include <dsn/threadpool/detail/infinite_counter.h>

using counter_t          = dsn::threadpool::detail::infinite_counter<int>;
using wrapping_counter_t = dsn::threadpool::detail::infinite_counter<int, 2>;

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE(default_construction) { counter_t counter; }

BOOST_AUTO_TEST_CASE(increment_operator)
{
    counter_t c1;
    counter_t c2 = ++c1;
    BOOST_REQUIRE((c1 > c2) == false);
    BOOST_REQUIRE((c2 > c1) == false);
}

BOOST_AUTO_TEST_CASE(no_increment)
{
    counter_t c1, c2;
    BOOST_REQUIRE((c1 > c2) == false);
    BOOST_REQUIRE((c2 > c1) == false);
}

BOOST_AUTO_TEST_CASE(increment_one)
{
    counter_t c1, c2;
    ++c1;
    BOOST_REQUIRE((c1 > c2) == true);
    BOOST_REQUIRE((c2 > c1) == false);
}

BOOST_AUTO_TEST_CASE(increment_both)
{
    counter_t c1, c2;
    ++c1;
    ++c2;

    BOOST_REQUIRE(!(c1 > c2));
    BOOST_REQUIRE(!(c2 > c1));

    ++c1;
    BOOST_REQUIRE(c1 > c2);
    BOOST_REQUIRE(!(c2 > c1));
}

BOOST_AUTO_TEST_CASE(increment_both_wrapping)
{
    wrapping_counter_t c1, c2;
    ++c1;
    ++c2;
    BOOST_REQUIRE(!(c1 > c2));
    BOOST_REQUIRE(!(c2 > c1));

    ++c1;
    ++c2;
    BOOST_REQUIRE(!(c1 > c2));
    BOOST_REQUIRE(!(c2 > c1));

    ++c1;
    BOOST_REQUIRE(c1 > c2);
    BOOST_REQUIRE(!(c2 > c1));
}

BOOST_AUTO_TEST_CASE(increment_one_wrapping)
{
    wrapping_counter_t c1, c2;
    ++c1;
    ++c2;
    ++c1;
    ++c1;

    BOOST_REQUIRE(c1 > c2);
    BOOST_REQUIRE(!(c2 > c1));
}
