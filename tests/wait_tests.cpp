#define BOOST_TEST_MODULE "dsn::threadpool wait.h"

#include <dsn/threadpool/pool.h>
#include <dsn/threadpool/wait.h>

#include <list>

using dsn::threadpool::pool;

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE(get_void_empty)
{
    std::vector<std::future<void>> futures;
    dsn::threadpool::get(futures.begin(), futures.end());
}

BOOST_AUTO_TEST_CASE(get_int_empty)
{
    std::vector<std::future<int>> futures;
    const auto                    result = dsn::threadpool::get(futures.begin(), futures.end());
    BOOST_REQUIRE(result.empty());
}

namespace {
const auto num_cores = std::thread::hardware_concurrency();
}

BOOST_AUTO_TEST_CASE(get_void)
{
    pool                           pool{ num_cores };
    std::vector<std::future<void>> futures;
    int                            a{ 0 }, b{ 0 };

    futures.emplace_back(pool.push([&a] { a = 1; }));
    futures.emplace_back(pool.push([&b] { b = 2; }));

    dsn::threadpool::get(futures.begin(), futures.end());

    BOOST_REQUIRE_EQUAL(a, 1);
    BOOST_REQUIRE_EQUAL(b, 2);
}

BOOST_AUTO_TEST_CASE(get_int)
{
    std::vector<std::future<int>> futures;
    pool                          pool{ num_cores };

    futures.emplace_back(pool.push([] { return 1; }));
    futures.emplace_back(pool.push([] { return 2; }));

    auto result = dsn::threadpool::get(futures.begin(), futures.end());
    BOOST_REQUIRE_EQUAL(futures.size(), result.size());
    BOOST_REQUIRE_EQUAL(result[0], 1);
    BOOST_REQUIRE_EQUAL(result[1], 2);
}

BOOST_AUTO_TEST_CASE(get_int_list)
{
    std::vector<std::future<int>> futures;
    pool                          pool{ num_cores };

    futures.emplace_back(pool.push([] { return 1; }));
    futures.emplace_back(pool.push([] { return 2; }));

    auto result = dsn::threadpool::get(futures.begin(), futures.end(), std::list<int>{});
    auto it     = result.begin();
    BOOST_REQUIRE_EQUAL(*it, 1);
    ++it;
    BOOST_REQUIRE_EQUAL(*it, 2);
}

BOOST_AUTO_TEST_CASE(pool_wait)
{
    pool                           pool{ num_cores };
    int                            a{ 0 };
    std::vector<std::future<void>> futures;

    futures.emplace_back(pool.push([&a]() {
        std::this_thread::sleep_for(std::chrono::milliseconds(250));
        a = 1;
    }));

    dsn::threadpool::wait(futures.begin(), futures.end());
    BOOST_REQUIRE_EQUAL(a, 1);
}

namespace {
class condvar {
private:
    bool                    m_flag;
    std::condition_variable m_cond;
    std::mutex              m_mutex;

    void make_flag_true()
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_flag = true;
    }

public:
    condvar()
        : m_flag{ false }
        , m_cond{}
        , m_mutex{}
    {
    }

    void notify_one()
    {
        make_flag_true();
        m_cond.notify_one();
    }

    void notify_all()
    {
        make_flag_true();
        m_cond.notify_all();
    }

    void wait()
    {
        std::unique_lock<std::mutex> lock(m_mutex);
        m_cond.wait(lock, [this]() { return m_flag; });
    }
};
}

BOOST_AUTO_TEST_CASE(pool_wait_for)
{
    pool                           pool{ num_cores };
    int                            a{ 0 };
    condvar                        cv;
    std::vector<std::future<void>> futures;

    futures.emplace_back(pool.push([&a, &cv] {
        cv.wait();
        a = 1;
    }));

    dsn::threadpool::wait_for(futures.begin(), futures.end(), std::chrono::milliseconds(50));
    BOOST_REQUIRE_EQUAL(a, 0);

    cv.notify_one();
    dsn::threadpool::wait_for(futures.begin(), futures.end(), std::chrono::milliseconds(250));
    BOOST_REQUIRE_EQUAL(a, 1);
}

BOOST_AUTO_TEST_CASE(pool_wait_until)
{
    pool                           pool{ num_cores };
    int                            a{ 0 };
    condvar                        cv;
    std::vector<std::future<void>> futures;
    auto                           now = std::chrono::steady_clock::now();

    futures.emplace_back(pool.push([&a, &cv] {
        cv.wait();
        a = 1;
    }));

    dsn::threadpool::wait_until(futures.begin(), futures.end(), now + std::chrono::milliseconds(100));
    BOOST_REQUIRE_EQUAL(a, 0);

    cv.notify_one();
    dsn::threadpool::wait_until(futures.begin(), futures.end(), now + std::chrono::milliseconds(200));
    BOOST_REQUIRE_EQUAL(a, 1);
}
