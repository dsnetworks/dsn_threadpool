#define BOOST_TEST_MODULE "dsn::threadpool::error"

#include <dsn/threadpool/error.h>

#include <boost/test/unit_test.hpp>

namespace {
void throw_test_error() { throw dsn::threadpool::error("expected testing exception"); }
}

BOOST_AUTO_TEST_CASE(construct_error) { BOOST_REQUIRE_THROW(throw_test_error(), dsn::threadpool::error); }

BOOST_AUTO_TEST_CASE(error_what) {
    const std::string test_message{"error::what test"};
    bool thrown{false};

    try {
        throw dsn::threadpool::error(test_message);
    }

    catch (dsn::threadpool::error& ex) {
        BOOST_REQUIRE(ex.what() == test_message);
        thrown = true;
    }

    BOOST_REQUIRE(thrown == true);
}
