#define BOOST_TEST_MODULE "dsn::threadpool::pool"

#include <dsn/threadpool/pool.h>

using dsn::threadpool::pool;

#include <boost/test/unit_test.hpp>

namespace {
const auto num_cores = std::thread::hardware_concurrency();
}

BOOST_AUTO_TEST_CASE(default_ctor)
{
    const pool pool;
    BOOST_REQUIRE_EQUAL(pool.num_threads(), 0);
}

BOOST_AUTO_TEST_CASE(construct_with_workers)
{
    const pool pool{ num_cores };
    BOOST_REQUIRE_EQUAL(pool.num_threads(), num_cores);
}

BOOST_AUTO_TEST_CASE(construct_and_add_workers)
{
    pool pool;
    BOOST_REQUIRE_EQUAL(pool.num_threads(), 0);

    pool.add_threads(num_cores);
    BOOST_REQUIRE_EQUAL(pool.num_threads(), num_cores);
}

BOOST_AUTO_TEST_CASE(push_simple)
{
    pool pool;
    int  a{ 1 };
    auto future = pool.push([&]() { a = 2; });
    BOOST_REQUIRE_EQUAL(pool.num_tasks(), 1);

    pool.add_threads(2);

    future.get();
    BOOST_REQUIRE_EQUAL(a, 2);
    BOOST_REQUIRE_EQUAL(pool.num_tasks(), 0);
}

BOOST_AUTO_TEST_CASE(push_simple_multiple)
{
    pool pool{ num_cores };

    auto f1 = pool.push([] { return 1; });
    auto f2 = pool.push([](double value) { return value * 2.0; }, 2.0);

    BOOST_REQUIRE_EQUAL(f1.get(), 1);
    BOOST_REQUIRE_EQUAL(f2.get(), 4.0);
}

BOOST_AUTO_TEST_CASE(push_with_prio)
{
    pool pool{ num_cores };

    auto f1 = pool.push([] { return 1; });
    auto f2 = pool.push(1, [](double value) { return value * 2.0; }, 2.0);
    auto f3 = pool.push(2, [](double a, int b) { return a * b; }, 3.0, 2);
    auto f4 = pool.push(1, [] { return true; });

    BOOST_REQUIRE_EQUAL(f1.get(), 1);
    BOOST_REQUIRE_EQUAL(f2.get(), 4.0);
    BOOST_REQUIRE_EQUAL(f3.get(), 6.0);
    BOOST_REQUIRE_EQUAL(f4.get(), true);
}

BOOST_AUTO_TEST_CASE(push_with_exception_in_task)
{
    pool pool{ num_cores };

    auto f1 = pool.push([]() -> int { throw std::invalid_argument("expected"); });
    auto f2 = pool.push([](double value) { return value * value; }, 2.0);

    BOOST_REQUIRE_THROW(f1.get(), std::invalid_argument);
    BOOST_REQUIRE_EQUAL(f2.get(), 4.0);
}

BOOST_AUTO_TEST_CASE(parallel_add_threads_and_num_threads)
{
    pool pool{ num_cores };
    for (auto i = 0; i < 20; ++i) {
        auto t1 = std::thread([&pool] { pool.add_threads(2); });
        auto t2 = std::thread([&pool] { BOOST_REQUIRE_GE(pool.num_threads(), num_cores); });
        t1.join();
        t2.join();
    }
}

BOOST_AUTO_TEST_CASE(pause_and_resume)
{
    pool pool;
    BOOST_REQUIRE_EQUAL(pool.is_paused(), false);

    auto f1 = pool.push([] { return 1; });
    auto f2 = pool.push([] { return 2; });

    pool.set_paused(true);
    BOOST_REQUIRE_EQUAL(pool.is_paused(), true);

    pool.add_threads(num_cores);
    BOOST_REQUIRE_EQUAL(pool.num_threads(), num_cores);

    pool.set_paused(false);
    BOOST_REQUIRE_EQUAL(pool.is_paused(), false);

    BOOST_REQUIRE_EQUAL(f1.get(), 1);
    BOOST_REQUIRE_EQUAL(f2.get(), 2);
}

BOOST_AUTO_TEST_CASE(clear_tasks)
{
    pool pool;

    pool.push([] { return 1; });
    pool.push([] { return 2; });
    BOOST_REQUIRE_EQUAL(pool.num_tasks(), 2);

    pool.clear();
    BOOST_REQUIRE_EQUAL(pool.num_tasks(), 0);
}
